// Load templates for a given document
// Example template in HTML:
// <div class='template' template='practice'></div>
// The 'class' and 'template' properties are required
// Also required is the practice.html object
const dir = 'word_freq';
const debug = false;


function log_debug(...msg) {
  if (debug) {
    console.log('DEBUG:', msg.join(' '));
  }
}


function RenderTemplates(root, name) {
  log_debug('Load templates:', name);
  const templates = root.getElementsByClassName('template');
  for (template of templates) {
    // Load each template
    // Fetch template content and append
    (async (t) => {
      let name = t.getAttribute('template');
      let path = dir + '/' + name + '.html';
      let t_data = await fetch(path)
        .then(response => response.text())
        .then(data => {
          return data;
        })
      t.innerHTML = t_data;
      log_debug('Loaded', path);


      // Load scripts
      if (t.hasAttribute('script')) {
        const script_path = 'js/' + dir + '/' + name + '.js';
        log_debug('Load script:', script_path);
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = script_path;
        head.appendChild(script); 
      }

      // Load sub-templates
      let parser = new DOMParser();
      let doc = parser.parseFromString(t_data, 'text/html');
      RenderTemplates(t, path);
    })(template);
  }
}
