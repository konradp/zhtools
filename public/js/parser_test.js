// include parser.js

function mouseOver(event) {
  var tip = _getId('tip');
  tip.style.setProperty('width', 'auto');
  tip.style.setProperty('white-space', 'nowrap');
  tip.innerHTML = this.getElementsByTagName('bc')[0].innerHTML;
  var chRect = this.getBoundingClientRect();
  var h = parseFloat(getComputedStyle(this).getPropertyValue('height').replace('px', ''));
  tip.style.setProperty('top', chRect.y + h);
  tip.style.setProperty('left', chRect.x);
  tip.style.setProperty('visibility', 'visible');
  var tipRect = tip.getBoundingClientRect();
  if (tipRect.width > window.innerWidth) {
    // Shrink tip: also enable wrap
    tip.style.setProperty('white-space', 'normal');
    var padding = parseFloat(getComputedStyle(tip).getPropertyValue('padding').replace('px',''));
    tip.style.setProperty('width', window.innerWidth - 2*padding);
  }
  var tipRect = tip.getBoundingClientRect();
  if (tipRect.right > window.innerWidth) {
    // Shift tip to the left
    tip.style.setProperty('left', tipRect.left - (tipRect.right - window.innerWidth));
  }
  var tipRect = tip.getBoundingClientRect();
  if (tipRect.left < 0) {
    // Shift tip to the right
    tip.style.setProperty('left', 0);
  }
}



// MAIN
var dict = [];
async function run() {
  let urlOfFile = 'data/' + _getId('select_dict').value;
  var parser = new Parser();
  parser.loadDictFromUrl(urlOfFile);


  return;

  var tip = document.createElement('div');
  tip.id = 'tip';
  document.getElementsByTagName('body')[0].appendChild(tip);

  const txt = _getId('txt').value;
  res = processTxt(txt);
  var out = _getId('out');
  for (k of res) {
    var ch = document.createElement('ch');
    if (typeof(k) == 'string') {
      ch.innerHTML = k;
    } else {
      // A translatable char
      ch.className = 'ch';
      ch.innerHTML = k[0]['chars'][0];
      var bc = document.createElement('bc');
      bc.style = 'visibility: hidden;position:absolute;';
      bc.innerHTML +=  '<span id="tip-title">' + k[0]['chars'].join(' / ') + '</span><br>';
      for (l of k) {
        bc.innerHTML += '<strong>' + l['pinyin'] + '</strong>';
        var ul = document.createElement('ul');
        for (meaning of l['meaning']) {
          var li = document.createElement('li');
          li.innerHTML = meaning;
          ul.appendChild(li);
        }
        bc.appendChild(ul);
      }
      ch.appendChild(bc);
      ch.addEventListener('mouseover', mouseOver, false);
      ch.addEventListener('mouseout', function(event) {
        var tip = _getId('tip');
        tip.style.setProperty('visibility', 'hidden');
      }, false);
    }
    out.appendChild(ch);
  } // end processTxt
}
