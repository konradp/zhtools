class DictParser {
  #dict = [];
  async Load(urlDict) {
    // Load dict from text and parse it
    console.log('Load dict', urlDict);
    for await (const line of this.makeDictIterator(urlDict)) {
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      this.#ParseLine(line);
    }
    console.log('Parsed dict');
    return this.#dict;
  }

  // PRIVATE

  #ParseLine(line) {
    if (line.startsWith('#')) {
      // Skip comments
      return;
    }
    let parsed = {
      'trad': '',
      'simp': '',
      'pinyin': '',
      'meanings': [],
    }
    // Char
    parsed.trad = line.split(' ')[0]
    parsed.simp = line.split(' ')[1]

    // Pinyin
    const a = line.indexOf('[');
    const b = line.indexOf(']');
    parsed.pinyin = line.substr(a+1, b-a-1)

    // Meanings
    parsed.meanings = line.split('/');
    parsed.meanings.shift();
    parsed.meanings.pop();

    // Append to main dict
    this.#dict.push(parsed);
  }

  async * makeDictIterator(fileURL) {
    // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
    const utf8Decoder = new TextDecoder('utf-8');
    const response = await fetch(fileURL);
    const reader = response.body.getReader();
    let { value: chunk, done: readerDone } = await reader.read();
    chunk = chunk ? utf8Decoder.decode(chunk) : '';

    //const re = /\n|\r|\r\n/gm;
    const re = /\r\n/g;
    let startIndex = 0;
    let result;

    while (true) {
      let result = re.exec(chunk);
      if (!result) {
        if (readerDone) break;
        let remainder = chunk.substr(startIndex);
        ({ value: chunk, done: readerDone } = await reader.read());
        chunk = remainder + (chunk ? utf8Decoder.decode(chunk) : '');
        startIndex = re.lastIndex = 0;
        continue;
      }
      yield chunk.substring(startIndex, result.index);
      startIndex = re.lastIndex;
    }
    if (startIndex < chunk.length) {
      // Last line didn't end in a newline char
      yield chunk.substr(startIndex);
    }
  }

}; // end class
