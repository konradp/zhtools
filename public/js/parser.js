class Parser {
// PUBLIC
loadDictFromUrl(url) {
  for (let line of this.makeTextFileLineIterator(url)) {
    var a = this.processDictLine(line);
    if (a != undefined) {
      dict.push(a);
    }
  }
}


// PRIVATE
getCandidates(str) {
  // For given string, get dictionary entries that start with that str
  return dict.filter(function(ch) {
      for (c of ch['chars']) {
        if (c.startsWith(str)) return true;
      }
      return false;
  });
}


getLongestExactMatch(txt, i) {
  var c = null;
  var j = 0;
  var allCandidates = [];
  while (true) {
    if (i + j >= txt.length) {
      // end of txt
      break;
    }
    if (j == 0) {
      var c = txt.charAt(i);
    } else {
      c += txt.charAt(i+j);
    }
    candidates = this.getCandidates(c);
    if (!candidates.length) {
      // No more matches. Get longest exact match from candidates
      // Note: Last element in the array are the longest matches
      return allCandidates.pop();
    } else {
      // Found candidates, so we know we will try longer matches
      // but for now, pick only exact matches
      candidates = candidates.filter(candidate => candidate['chars'].includes(c));
      if (candidates.length) {
        allCandidates.push(candidates);
      }
    }
    j++;
  }
}


*makeTextFileLineIterator(fileURL) {
  // source: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  const utf8Decoder = new TextDecoder('utf-8');
  const response = fetch(fileURL);
  const reader = response.body.getReader();
  let { value: chunk, done: readerDone } = reader.read();
  chunk = chunk ? utf8Decoder.decode(chunk) : '';

  const re = /\r\n/gm;
  let startIndex = 0;
  let result;

  for (;;) {
    let result = re.exec(chunk);
    if (!result) {
      if (readerDone) {
        break;
      }
      let remainder = chunk.substr(startIndex);
      ({ value: chunk, done: readerDone } = reader.read());
      chunk = remainder + (chunk ? utf8Decoder.decode(chunk) : '');
      startIndex = re.lastIndex = 0;
      continue;
    }
    yield chunk.substring(startIndex, result.index);
    startIndex = re.lastIndex;
  }
  if (startIndex < chunk.length) {
    // last line didn't end in a newline char
    yield chunk.substr(startIndex);
  }
}


processDictLine(line) {
  // Process dictionary line (U8 format, like CC-CEDICT)
  // out: arrays: chars, pinyin, meaning
  if (line.startsWith('#')
    || line.startsWith('%')
  ) {
    return;
  }
  const a = line.indexOf(' [');
  const b = line.indexOf('] ');
  const res = {
    'chars': line.substring(a, 0).split(' '),
    'pinyin': line.substring(a+2, b),
    'meaning': line.substring(b+3, line.length-1).split('/'),
  };
  return res;
}


processTxt(txt) {
  var decomposed = [];
  var i = 0;
  while (true) {
    if (i >= txt.length) {
      // Reached end of txt
      break;
    }
    match = this.getLongestExactMatch(txt, i);
    if (match == undefined) {
      decomposed.push(txt.charAt(i));
      i++;
    } else {
      len = match[0]['chars'][0].length;
      decomposed.push(match);
      i = i + len;
    }
  }
  return decomposed;
}
}; // end class




function mouseOver(event) {
  var tip = _getId('tip');
  tip.style.setProperty('width', 'auto');
  tip.style.setProperty('white-space', 'nowrap');
  tip.innerHTML = this.getElementsByTagName('bc')[0].innerHTML;
  var chRect = this.getBoundingClientRect();
  var h = parseFloat(getComputedStyle(this).getPropertyValue('height').replace('px', ''));
  tip.style.setProperty('top', chRect.y + h);
  tip.style.setProperty('left', chRect.x);
  tip.style.setProperty('visibility', 'visible');
  var tipRect = tip.getBoundingClientRect();
  if (tipRect.width > window.innerWidth) {
    // Shrink tip: also enable wrap
    tip.style.setProperty('white-space', 'normal');
    var padding = parseFloat(getComputedStyle(tip).getPropertyValue('padding').replace('px',''));
    tip.style.setProperty('width', window.innerWidth - 2*padding);
  }
  var tipRect = tip.getBoundingClientRect();
  if (tipRect.right > window.innerWidth) {
    // Shift tip to the left
    tip.style.setProperty('left', tipRect.left - (tipRect.right - window.innerWidth));
  }
  var tipRect = tip.getBoundingClientRect();
  if (tipRect.left < 0) {
    // Shift tip to the right
    tip.style.setProperty('left', 0);
  }
}







// MAIN
var dict = [];
async function run() {
  let urlOfFile = 'data/' + _getId('select_dict').value;
  var parser = new Parser();
  parser.loadDictFromUrl(urlOfFile);

  const txt = _getId('txt').value;
  res = parser.processTxt(txt);

  // The rest
  var tip = document.createElement('div');
  tip.id = 'tip';
  document.getElementsByTagName('body')[0].appendChild(tip);

  var out = _getId('out');
  for (k of res) {
    var ch = document.createElement('ch');
    if (typeof(k) == 'string') {
      ch.innerHTML = k;
    } else {
      // A translatable char
      ch.className = 'ch';
      ch.innerHTML = k[0]['chars'][0];
      var bc = document.createElement('bc');
      bc.style = 'visibility: hidden;position:absolute;';
      bc.innerHTML +=  '<span id="tip-title">' + k[0]['chars'].join(' / ') + '</span><br>';
      for (l of k) {
        bc.innerHTML += '<strong>' + l['pinyin'] + '</strong>';
        var ul = document.createElement('ul');
        for (meaning of l['meaning']) {
          var li = document.createElement('li');
          li.innerHTML = meaning;
          ul.appendChild(li);
        }
        bc.appendChild(ul);
      }
      ch.appendChild(bc);
      ch.addEventListener('mouseover', mouseOver, false);
      ch.addEventListener('mouseout', function(event) {
        var tip = _getId('tip');
        tip.style.setProperty('visibility', 'hidden');
      }, false);
    }
    out.appendChild(ch);
  } // end processTxt
}
