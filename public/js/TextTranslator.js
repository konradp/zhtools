class TextTranslator {
  // Translate given text
  // text = "好久不見"
  // OR
  // text = [ '好', '久', '不', '見' ]
  #dict = [];
  #text = '';
  #result = [];
  #version = 1;

  constructor(dict) {
    this.#dict = dict;
  }
  SetText(text) {
    this.#text = text;
  }
  SetVersion(version) {
    this.#version = version;
  }
  _isCharMandarin(ch) {
    const excludeChars = [
      ' ', '', '\n',
      ':', '：', ';', '；', '。', '.', '，', ',', '、', "'",
      '/', '(', ')', '!', '！', '&',
      '」', '「', '（', '）', '-'
    ];
    if (ch.match(/[a-z]/i)
      || ch.match(/[0-9]/)
    ) {
      // Skip letters and numbers
      return false;
    }
    if (excludeChars.includes(ch)) {
      // Skip excluded chars (punctuation)
      return false;
    }
    return true;
  }


  Translate() {
    if (this.#text == '') {
      throw 'No text supplied to translate';
    }
    const dict = this.#dict;
    if (dict.length == 0) {
      throw 'Missing dictionary';
    }
    switch (this.#version) {
      case '1':
      default:
        // Basic, find longest match
        return this.#TranslateV1();
        break;
      case '2':
        // My version: Find all matches
        return this.#TranslateV2();
        break;
      case '3':
        // Individual chars
        return this.#TranslateV3();
        break;
    }
  }


  #TranslateV1() {
    // The basic translation:
    // - find the longest meaning that starts with given char
    // WARN: Doesn't work if text is array!
    const text = this.#text;
    let text_translated = [];
    var i = 0;
    while (i < text.length) {
      let candidates_dict = this.#GetDefsStartWith(text[i]);
      if (candidates_dict.length == 0 || !_isCharMandarin(text[i])) {
        text_translated.push({char: text[i]});
        i++;
        continue;
      }
      let max_len = Math.max(...candidates_dict.map(el => el.trad.length));
      let matches = [];
      for (var j = 1; j <= max_len; j++) {
        matches.push(text.slice(i, i+j));
      }
      let candidates_match = candidates_dict.filter(el => {
        return matches.includes(el.trad);
      });
      if (candidates_match.length == 0) {
        text_translated.push({char: text[i]});
        i++;
        continue;
      }
      max_len = Math.max(...candidates_match.map(el => el.trad.length));
      candidates_match = candidates_match.filter(el => {
        return el.trad.length == max_len;
      });
      text_translated.push({
        char: text.slice(i, i+max_len),
        meanings: candidates_match,
      });
      i += max_len;
    }
    return text_translated;
  }


  #TranslateV2() {
    // for each char
    // look up defs that start with given char
    // Get all words that start with the current char
    // (including the longest such word),
    // Then, pick only the words which match the text
    // (but also avoid going past the end of the text)
    const text = this.#text;
    let candidates = [];
    let text_translated = [];
    for (var i = 0; i < text.length; i++) {
      candidates = this.#GetDefsStartWith(text[i]);
      const max_l = Math.max(...candidates.map(el => el.trad.length));
      var options = [];
      for (var j = 0; j < Math.min(max_l, text.length-i); j++) {
        options = options.concat(candidates.filter((el) => {
          return el.trad == text.slice(i, i+j+1)
        }));
      }
      text_translated.push({
        char: text[i],
        options: options,
      });
    }
    return text_translated;
  }


  #TranslateV3() {
    // Translate each char separately
    const text = this.#text;
    let candidates = [];
    let text_translated = [];
    for (var i = 0; i < text.length; i++) {
      let ch = text[i];
      const meanings = this.#GetDefsExact(ch);
      if (meanings.length > 0 && this._isCharMandarin(ch)) {
        text_translated.push({
          char: ch,
          meanings: this.#GetDefsExact(ch),
        });
      } else {
        text_translated.push({ char: ch });
      }
    }
    return text_translated;
  }


  #GetDefsStartWith(str) {
    const dict = this.#dict;
    return dict.filter(entry => entry.trad.startsWith(str));
  }


  #GetDefsExact(str) {
    const dict = this.#dict;
    return dict.filter(entry => entry.trad == str);
  }


}; // end class
