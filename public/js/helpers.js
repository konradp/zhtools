function _showHideElement(el) {
  if (el.style.display == 'none') {
    el.style.display = 'block';
  } else {
    el.style.display = 'none';
  }
}


function _isCharMandarin(ch) {
  const excludeChars = [
    ' ', '', '\n',
    ':', '：', ';', '；', '。', '.', '，', ',', '、', "'",
    '/', '(', ')', '!', '！', '&',
    '」', '「', '（', '）', '-'
  ];
  if (ch.match(/[a-z]/i)
    || ch.match(/[0-9]/)
  ) {
    // Skip letters and numbers
    return false;
  }
  if (excludeChars.includes(ch)) {
    // Skip excluded chars (punctuation)
    return false;
  }
  return true;
}


function _deactivateTabHeader(tab) {
  tab.className = tab.className.replace(' tab-active', '');
}


function _getId(id) {
  return document.getElementById(id);
}


function _getChar(ch, arr) {
  // ch = '我'
  // arr = [ { char:'我', meanings: [] }, ... ]
  return arr.filter(el => el.char == ch)[0];
}

function _getRss() {
  // RSS feeds
  //const url = 'https://feeds.bbci.co.uk/zhongwen/trad/rss.xml';
  // const url = 'https://www.bbc.com/zhongwen/trad/services/2009/09/000000_rss';
  //const url = 'https://news.ltn.com.tw/rss/all.xml';
  // const url = 'http://southasiawatch.tw/feed/';
  // const url = 'https://www.zhihu.com/rss';
  const proxyUrl = 'https://api.rss2json.com/v1/api.json?rss_url=';
  const url = proxyUrl + 'https://feeds.bbci.co.uk/zhongwen/trad/rss.xml';
  fetch(url)
    .then((response => response.json()))
    .then(data => {
      console.log(data);
    })
}

function _md5(str) {
  // Requires CryptoJS
  return CryptoJS.MD5(str).toString();
}

function _generateTextTitle(text) {
  // Generate title from a text (first five Mandarin characters)
  let title = '';
  for (let i = 0; i < text.length && title.length < 5; i++) {
    if (_isCharMandarin(text[i])) {
      title += text[i];
    }
  }
  if (title == '') {
    title = 'Untitled';
  }
  return title;
}

function _downloadJsonFile(name, data) {
  const file = new File([JSON.stringify(data, null, 2)], name, {
    type: 'application/json',
  });
  const link = document.createElement('a');
  link.href = URL.createObjectURL(file);
  link.download = file.name;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);

}
