// requires:
// - app_word_freq_templates.js

// Variables
let dict = [];
let dict_loaded = false;
let translatedWords = [];
let translatedChars = [];
let commonChars = {};  // [ '一', '丁', '七', ... ]
let chars_chosen = []; // chosen to study
let dom_chars = {};
let dom_chars_chosen = {};
let uniqueWords = [];
let uniqueChars = [];
let studyBook = {};
let studyBookName = '';
let isStudybookLoaded = false;
let studyBookConfirm = "\
The study book is not loaded. Continue nonetheless? \n\
\n\
Loading a study book will allow to save the study progress after a study \
session. The studybook will remember the already learned characters. \n\
\n\
Load a study book: Press 'Cancel', and then press 'Load studybook' on the right. \n\
Create a new study book: Press 'Cancel', go to the 'About' tab, and press \
'New studybook'. Then, load the new study book. \n\
Continue: Press 'OK' to analyse the text anyway. There will be no option to \
save the learned characters. \
";


function CreateTooltip() {
  // The tooltip is a hovering div, hidden at first
  const tip = document.createElement('div');
  tip.id = 'tip';
  tip.className = 'tip';
  tip.innerHTML = '我';
  document.getElementsByTagName('body')[0].appendChild(tip);
}


function OnLoad() {
  // Page load, init
  // Load templates (requires app_word_freq_templates.js)
  RenderTemplates(document, 'main');

  (async () => {
    // Fetch common_chars, and convert to an object of structs
    await fetch('data/common_chars.json')
      .then(response => response.json())
      .then(data => {
        // Read commonChars
        commonChars = data;
      })
  })();

  // Load dict and parse it
  const dict_parser = new DictParser();
  dict_parser.Load('data/cedict_ts.u8')
    .then((data) => {
      dict = data;
      dict_loaded = true;
  });
  CreateTooltip();

  // Studybook loader
  const inputElement = _getId('studybook-upload');
  inputElement.addEventListener('change', studybookLoad, false);
  function studybookLoad() {
    const file = this.files[0];
    studyBookName = file.name;
    let reader = new FileReader();
    reader.readAsText(file,'UTF-8');
    reader.onload = readerEvent => {
      studyBook = JSON.parse(readerEvent.target.result);
      isStudybookLoaded = true;
    }
  }
}


function _showHideOverlay(name) {
  let div = _getId(name);
  // Disable scrolling background
  let body = _getId('body');
  if (body.className == '') {
    body.className = 'no-scroll';
  } else {
    body.className = '';
  }
  _showHideElement(div);
}


////// MAIN BUTTONS
// SECTION: TEXT
function Analyse() {
  // Analyse: Main
  if (!dict_loaded) {
    alert('Dict is not loaded yet. Please try again in a few seconds...');
    return;
  }

  if (!isStudybookLoaded) {
    if (!window.confirm(studyBookConfirm)) {
      return;
    }
  }

  // Take the textarea from 'Edit' and paste into 'View'
  let editText = _getId('study-edit-text');

  // Buttons: Show 'view', hide 'edit'
  let edit = _getId('study-edit');
  let view = _getId('study-view');
  edit.style = 'display: none';
  view.style = 'display: block';

  // Translate words
  const text_translator = new TextTranslator(dict);
  text_translator.SetVersion('1');
  text_translator.SetText(editText.value);
  translatedWords = text_translator.Translate();
  uniqueWords = translatedWords.filter((el) => {
    return el.hasOwnProperty('meanings');
  });
  // Translate by char
  text_translator.SetVersion('3');
  translatedChars = text_translator.Translate();
  let tmpUniqueChars = translatedChars.filter((el) => {
    return el.hasOwnProperty('meanings');
  });
  // Get distinct chars
  for (ch of tmpUniqueChars) {
    if (uniqueChars.filter(e=>e.char==ch.char) == 0) {
      ch.count = 0;
      uniqueChars.push(ch);
    }
    uniqueChars.filter(e=>e.char == ch.char)[0].count++;
  }

  // Stats: calculate
  text = _getId('study-edit-text').value;
  countChar = 0;
  countCharMandarin = 0;
  // Generate unique char stats, skip non-mandarin chars
  for (let i = 0; i < text.length; i++) {
    if (_isCharMandarin(text.charAt(i))) {
      countCharMandarin++;
    }
  }
  // How many of the chars are common
  countCommonOccurrences = 0;
  for (ch of uniqueChars.map(el => el.char)) {
    if (commonChars.includes(ch)) {
      countCommonOccurrences++;
    }
  }

  // Stats: draw
  table = {
    'chars': _getId('study-edit-text').value.length,
    'charsmandarin': countCharMandarin,
    'charsuniq': uniqueChars.length,
    'comchars': Object.keys(commonChars).length,
    'comcharsfreq': countCommonOccurrences,
  };
  for (name in table) {
    _getId(name).innerHTML = table[name];
  }

  // Annotate
  RenderResultText(translatedWords);
  InitPickerWords();
  InitPickerChars();

  // Reset other values
  chars_chosen = [];
}


function Edit() {
  // Show 'edit', hide 'view'
  _getId('study-edit').style = 'display:block';
  _getId('study-view').style = 'display:none';
}


function RenderResultText(text) {
  // translated: [
  //   [
  //     {char: 去, meanings: [ { trad, meanings: [], pinyin } ],},
  //     {char: 中國, meanings: [...],}
  //   ]
  // or
  // translated: [
  // [
  //   { char: "中", options: [...] },
  //   { char: "國", options: [...] },
  // ]
  const result = _getId('study-view-text');
  result.innerHTML = '';
  let i = 0;
  for (c of text) {
    let ch = document.createElement('ch');
    ch.innerHTML = c.char;
    ch.id = i;
    result.appendChild(ch);

    // Mouse actions
    if (c.hasOwnProperty('meanings')) {
      ch.addEventListener('mouseover', mouseOverWord, false);
      ch.addEventListener('mouseout', mouseOutWord, false);
    }
    i++;
  }
}


function Tab(name) {
  // Switch to a different text tab: study/practice/about
  // uses CSS classes
  // - name:
  //   - study
  //   - practice
  //   - about
  // Check
  t_active = _getId(name);
  if (t_active == null) {
    console.log('ERROR: Tab ' + name + ' not found!');
    return;
  }
  // Hide all other tabs and show current
  t_contents = document.getElementsByClassName('tab-content');
  for (i = 0; i < t_contents.length; i++) {
    t_contents[i].style.display = 'none';
  }
  t_active.style.display = 'block';
  // Buttons: deactivate all
  let tabs = document.getElementsByClassName('tab');
  for (i = 0; i < tabs.length; i++) {
    tabs[i].className = tabs[i].className.replace(' tab-active', '');
  }
  // Buttons: activate current
  current = _getId('tab-btn-' + name);
  current.className += ' tab-active';
}


function StatsShowHide() {
  // Show/hide Stats overlay
  _showHideOverlay('study-stats');
}
