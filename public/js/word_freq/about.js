const btnNewStudybook = _getId('new-studybook');
btnNewStudybook.addEventListener('click', onNewStudyBookClicked, false);

function onNewStudyBookClicked() {
  // Create new studybook and save it
  let name = prompt('Enter name');
  if (name != null) {
    let data = {
      title: 'Studybook for koradp.gitlab.io/zhtools',
      name: name,
      study_sessions: [],
      texts: [],
    };
    _downloadJsonFile('studybook-' + name + '.json', data);
  }
}
