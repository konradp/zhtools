// This does:
// - tooltip actions

const btnSaveStudybook = _getId('save-studybook');
btnSaveStudybook.addEventListener('click', saveToStudybook, false);

function saveToStudybook() {
  if (chars_chosen.length == 0) {
    alert('Select chars to study first, and practice them.');
    return;
  }
  let text = _getId('study-edit-text').value;
  let chars = chars_chosen.map(c=>c.char);
  let textHash = _md5(text);
  let date = Date.now();

  if (studyBook.texts.filter(e=>e.id==textHash).length == 0) {
    // Add text to studybook if not yet present
    console.log('Adding new text to studybook');
    studyBook.texts.push({
      id: textHash,
      title: _generateTextTitle(text),
      content: text,
      type: 'pasted', // TODO: handle other text types
      date: date,
    });
  }
  let textObj = studyBook.texts.filter(e=>e.id==textHash)[0];
  // Append session
  studyBook.study_sessions.push({
    date: date,
    textId: textHash,
    charsPicked: chars,
  });
  // Download file
  _downloadJsonFile(studyBookName, studyBook);
}

function mouseOverChar() {
  let ch = uniqueChars.filter(e=>e.char==this.innerHTML)[0];
  this.style.setProperty('background-color', 'gray');
  populateTip(this, ch, event);
}

function mouseOverWord() {
  const ch = uniqueWords.filter(e=>e.char == this.innerHTML)[0];
  this.style.setProperty('background-color', 'gray');
  populateTip(this, ch);
}

function mouseOutChar() {
  const tip = _getId('tip');
  tip.style.setProperty('visibility', 'hidden');
  this.style.removeProperty('background-color');
}


function mouseOutWord() {
  // this: ch span
  const tip = _getId('tip');
  tip.style.setProperty('visibility', 'hidden');
  // Unhighlight word in text
  this.style.removeProperty('background-color');
}


function populateTip(sourceElement, ch) {
  // sourceElement: the char span that mouse was hovered on
  if (ch.hasOwnProperty('meanings')) {
    // Translatable char
    const d = document;
    const tip = _getId('tip');
    tip.innerHTML = '';
    tip.style.setProperty('visibility', 'visible');

    // Title: char
    let title = d.createElement('div');
    title.className = 'tip-title';
    title.textContent = ch.char;
    tip.appendChild(title);

    for (meaning of ch.meanings) {
      // Pinyin
      let p = d.createElement('div');
      p.className = 'tip-pinyin';
      p.textContent = meaning.pinyin;
      tip.appendChild(p);

      // Meanings
      var u = d.createElement('ul');
      u.className = 'tip-meanings';
      tip.appendChild(u);
      for (def of meaning.meanings) {
        var l = d.createElement('li');
        l.textContent = def;
        u.appendChild(l);
      }
    }

    // Tip position
    const chRect = sourceElement.getBoundingClientRect(); //x, y
    var tipRect = tip.getBoundingClientRect();
    tip.style.setProperty('top', chRect.bottom);
    tip.style.setProperty('left', chRect.left);

    //if (tipRect.width > window.innerWidth) {
    //  // Shrink tip
    //  tip.style.setProperty('white-space', 'normal');
    //  tip.style.setProperty('width', window.innerWidth);
    //}
    tipRect = tip.getBoundingClientRect();
    //if (tipRect.right > window.innerWidth) {
    const windowWidth = document.documentElement.clientWidth;
    if (tipRect.right > windowWidth) {
      // Shift tip to the left
      tip.style.setProperty('left', tipRect.left - (tipRect.right - windowWidth) + 'px');
    }
    //tipRect = tip.getBoundingClientRect();
    //if (tipRect.left < 0) {
    //  // Shift tip to the right
    //  tip.style.setProperty('left', 0);
    //}
  }
}
