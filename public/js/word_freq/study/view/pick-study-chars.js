let commonCharsCount = 1499;

// Hooks: sliders
const commonSlide = _getId('common_slide');
commonSlide.addEventListener('input', (event) => {
  // Update counts and show/hide chars
  const result = _getId('common_result');
  result.value = `${event.target.value}`;
  onSliderCommonChars();
});
const commonInput = _getId('common_result');
commonInput.addEventListener('input', (event) => {
  const result = _getId('common_slide');
  result.value = `${event.target.value}`;
  onSliderCommonChars();
});


function ShowPickerTab(tab) {
  // Flip between 'chars' or 'words' tab on char picker
  // Tab headers: hide both and highlight new tab
  _deactivateTabHeader(_getId('tab_pick_chars'));
  _deactivateTabHeader(_getId('tab_pick_words'));
  _getId('tab_'+tab).className += ' tab-active';
  // Contents: hide both and show the new tab
  _getId('pick_chars').style.display = 'none';
  _getId('pick_words').style.display = 'none';
  _getId(tab).style.display = 'block';
}


function InitPickerWords() {
  var table = _getId('pick_words');
  table.textContent = '';

  let word_cols = [];
  let arr = [...uniqueWords];
  for (let i = 4; i > 0; i--) {
    word_cols.push(arr.splice(0, Math.ceil(arr.length/i)));
  }

  for (col of word_cols) {
    let col_div = document.createElement('div');
    col_div.className = 'column';
    for (word of col) {
      let chSpan = document.createElement('span');
      chSpan.className = 'zhchar';
      chSpan.innerHTML = word.char;
      chSpan.id = word.char;
      chSpan.addEventListener('click', (event) => {
        onCharToStudyClicked(event.target.innerHTML);
      });
      chSpan.addEventListener('mouseover', mouseOverWord, false);
      chSpan.addEventListener('mouseout', mouseOutWord, false);
      col_div.appendChild(chSpan);
      col_div.appendChild(document.createElement('br'));
    }
    table.appendChild(col_div);
  }
}


function InitPickerChars() {
  // Used once. Draw char picker and pre-draw all chars.
  // The slider then hides/shows the chars.
  // Creates also chars_chosen

  // Char picker
  var table = _getId('tbl_pick_chars');
  table.textContent = '';
  const freqs = [...new Set(uniqueChars.map(e => e.count))];
  freqs.sort((a,b) => b-a);
  for (freq of freqs) {
    // Row for each frequency
    row = document.createElement('tr');
    td_freq = document.createElement('td');
      td_freq.className = 'td_right';
      td_freq.innerHTML = freq;
      row.appendChild(td_freq);
    var td_chars = document.createElement('td');
      td_chars.id = 'td_chars' + freq;
      row.appendChild(td_chars);
    table.appendChild(row);

    // Characters by frequencies
    for (ch of uniqueChars.filter(e => e.count == freq)) {
      // Available chars: append char
      let chSpan = document.createElement('span');
      chSpan.className = 'zhchar';
      chSpan.innerHTML = ch.char;
      chSpan.addEventListener('click', (event) => {
        onCharToStudyClicked(event.target.innerHTML);
      });
      chSpan.addEventListener('mouseover', mouseOverChar, false);
      chSpan.addEventListener('mouseout', mouseOutChar, false);
      dom_chars[ch.char] = chSpan;
      td_chars.appendChild(chSpan);
    }
  }

  // Chosen chars and words - pre-draw all hidden
  var div_chosen = _getId('chars_study_choice');
  div_chosen.innerHTML = '';
  for (ch of uniqueChars.concat(uniqueWords)) {
    var tr = document.createElement('tr'),
        ch_span = document.createElement('td'),
        btn_del = document.createElement('td'),
        img = document.createElement('img');
    // Char
    ch_span.innerHTML = ch.char;
    ch_span.className = 'zhchar';
    tr.style.display='none';
    tr.style.border = 'solid 1px';
    tr.appendChild(ch_span);
    // btn: Remove
    btn_del.ch = ch.char;
    btn_del.style.width = '1%';
    img.src = 'img/trash.svg';
    img.character = ch.char;
    btn_del.addEventListener('click', (event) => {
      addRemoveStudyChar(event.target.character);
    });
    btn_del.appendChild(img);
    tr.appendChild(btn_del);
    dom_chars_chosen[ch.char] = tr;
    div_chosen.appendChild(tr);
  }
}


function addRemoveStudyChar(ch) {
  // Study char (or word) has beeen clicked (or Add button)
  // Add it to list (or remove it)
  var dom_ch = dom_chars_chosen[ch];
  var display = dom_ch.style.display;
  if (display == 'none') {
    // Add
    dom_ch.style.display = '';
    chars_chosen.push({
      'char': ch,
      'score': 0
    });
  } else {
    // Remove
    dom_ch.style.display = 'none';
    chars_chosen = chars_chosen.filter(e => e['char'] !== ch)
  }
}


function SubmitCharsToStudy() {
  // Animate char strokes
  list = _getId('char-strokes');
  list.innerHTML = '';
  chars_chosen.forEach((word) => {
    let div_word = document.createElement('div');
    list.appendChild(div_word);
    for (ch of word['char']) {
      let div_ch = document.createElement('div');
      div_ch.id = ch + '-stroke';
      div_ch.style.display = 'inline-block';
      div_word.appendChild(div_ch);
      var writer = HanziWriter.create(ch + '-stroke', ch, {
        width: 100,
        height: 100,
        padding: 5,
        strokeAnimationSpeed: 2,
        delayBetweenStrokes: 10,
        showOutline: true
      });
      writer.loopCharacterAnimation();
    }
  });
}


function onCharToStudyClicked(ch) {
  // Study char has been clicked
  // On mobile, append 'Add' button to tooltip
  // On PC, simply clicking adds/removes char from studylist
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    // Mobile: Show 'Add' button'
    var is_displayed = dom_chars_chosen[ch].style.display;
    if (is_displayed == 'none') {
      var a = _getId('tip');
      var b = document.createElement('button');
      b.innerHTML = 'Add';
      b.addEventListener('mouseover', (event) => {
        addRemoveStudyChar(ch);
      });
      a.appendChild(document.createElement('hr'));
      a.appendChild(b);
    }
  } else {
    // PC, clicking just flips
    addRemoveStudyChar(ch);
  }
}


function onSliderCommonChars() {
  function isCharInFirstCommon(ch, count) {
    return commonChars.slice(0, count).includes(ch);
  }
  // Creates also Chars To Study
  // Clear
  for (ch of uniqueChars) {
    let ch_span = dom_chars[ch.char];
    const commonCharsCount = _getId('common_slide').value;
    if (!commonChars.slice(0, commonCharsCount).includes(ch.char)) {
      ch_span.style.display = 'none';
    } else {
      ch_span.style.display = '';
    }
  }
}
