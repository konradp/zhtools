let currentCardType = 'char';
// How many times to repeat a char
let score_max_for_char = 5;
let score_total = 0;

// Hooks: flashcard buttons
const btnShow = _getId('test_btn_show');
const btnYes = _getId('test_btn_yes');
const btnNo = _getId('test_btn_no');
btnShow.addEventListener('click', (event) => {
  FlipCardType();
  ShowCard();
});
btnYes.addEventListener('click', () => { TestAnswer('yes'); })
btnNo.addEventListener('click', () => { TestAnswer('no'); })

// Hooks: flashcard keys
document.addEventListener('keydown', event => {
  // If in test mode, override arrow keys, otherwise default
  let overlay = _getId('test_overlay');
  if (overlay.style.display == 'block') {
    event.preventDefault();
    if (event.isComposing || event.keyCode === 229) {
      return;
    }
    if (currentCardType == 'char') {
      // char: any arrow is 'show'
      if (['ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown'].includes(event.key)) {
        FlipCardType();
        ShowCard();
      }
    } else {
      // meaning: submit answer
      switch (event.key) {
        case 'ArrowLeft':
          TestAnswer('no'); break;
        case 'ArrowRight':
          TestAnswer('yes'); break;
        default: break;
      }
    }
  }
});


function FlashcardsShowHide() {
  // Show Flashcards overlay
  if (chars_chosen.length == 0) {
    // Check if chars_chosen are present
    alert('Select chars to study first.');
    return;
  }
  // Zero all the values
  index = 0;
  score_total = 0;
  for (ch in chars_chosen) {
    chars_chosen[ch]['score'] = 0;
  }
  score_max = score_max_for_char*chars_chosen.length;
  // Variables (used by child functions)
  currentCardType = 'char';
  _showHideOverlay('test_overlay');
  RenderIndex();
  ShowCard();
}


function FlashcardsHelpShowHide() {
  var t = _getId('flashcards_help');
  _showHideElement(t);
}


function FlipCardType() {
  if (currentCardType == 'char') {
    currentCardType = 'meaning';
  } else {
    currentCardType = 'char';
  }
}


function RenderIndex() {
  // progress, % score
  const divProgress = _getId('progress');
  divProgress.innerHTML = Math.round((score_total/score_max)*100) + '%';
}


function ShowCard() {
  const d = document,
        dict = uniqueWords.concat(uniqueChars),
        divFront = _getId('test_front'),
        divBack = _getId('test_back'),
        front = _getId('front'),
        back = _getId('back');
  if (currentCardType == 'char') {
    // Render both cards for the new char
    const ch = _getChar(chars_chosen[index].char, dict);
    divFront.innerHTML = ch.char;

    // Back: meanings
    divBack.innerHTML = '';
    let title = d.createElement('div');
    title.className = 'tip-title';
    title.textContent = ch.char;
    divBack.appendChild(title);

    for (meaning of ch.meanings) {
      // Pinyin
      let p = d.createElement('div');
      p.className = 'tip-pinyin';
      p.textContent = meaning.pinyin;
      divBack.appendChild(p);

      // Meanings
      var u = d.createElement('ul');
      u.className = 'tip-meanings';
      divBack.appendChild(u);
      for (def of meaning.meanings) {
        var l = d.createElement('li');
        l.textContent = def;
        u.appendChild(l);
      }
    }

    // Show/hide
    front.style.display = 'block';
    back.style.display = 'none';
  } else if (currentCardType == 'meaning') {
    front.style.display = 'none';
    back.style.display = 'inline-block';
  } else {
    console.error('Unknown card type', kind);
  }
}


function TestAnswer(value) {
  // Depending on 'yes' or 'no', increase, or decrease score for each char
  var score = chars_chosen[index]['score'];
  if (value == 'yes' && score < score_max_for_char) {
    chars_chosen[index]['score']++;
  } else if (value == 'no' && score > 0) {
    chars_chosen[index]['score']--;
  }

  // Total score
  score_total = Object.values(chars_chosen)
    .map(e=>e.score)
    .reduce((acc,val) => acc+val, 0);

  // Advance index, show next char card
  if (index+1 < chars_chosen.length) {
    index++;
  } else {
    index = 0;
  }

  if (score_total == score_max) {
    // All chars have been practiced, exit flashcards
    FlashcardsShowHide();
  }

  // Render next char
  RenderIndex();
  FlipCardType();
  ShowCard();
}
