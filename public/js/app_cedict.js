// Requires a div with id of 'result'
let dict = [];
let dict_loaded = false;
let translated = [];


function CreateTooltip() {
  // The tooltip is a hovering div, hidden at first
  const tip = document.createElement('div');
  tip.id = 'tip';
  tip.className = 'tip';
  tip.innerHTML = '我';
  document.getElementsByTagName('body')[0].appendChild(tip);
}

function mouseOver(event) {
  // TODO: algo 2
  console.log(translated[this.id]);
  const ch = translated[this.id];
  if (ch.hasOwnProperty('meanings')) {
    // Translatable char
    const d = document;
    const tip = _getId('tip');
    tip.innerHTML = '';
    tip.style.setProperty('visibility', 'visible');

    // Title: char
    let title = d.createElement('div');
    title.className = 'tip-title';
    title.textContent = ch.char;
    tip.appendChild(title);

    for (meaning of ch.meanings) {
      // Pinyin
      let p = d.createElement('div');
      p.className = 'tip-pinyin';
      p.textContent = meaning.pinyin;
      tip.appendChild(p);

      // Meanings
      var u = d.createElement('ul');
      u.className = 'tip-meanings';
      tip.appendChild(u);
      for (def of meaning.meanings) {
        var l = d.createElement('li');
        l.textContent = def;
        u.appendChild(l);
      }
    }

    // Tip position
    const chRect = this.getBoundingClientRect(); //x, y
    var tipRect = tip.getBoundingClientRect();
    var charHeight = parseFloat(getComputedStyle(this).getPropertyValue('height').replace('px', ''));
    tip.style.setProperty('top', chRect.y + charHeight);
    tip.style.setProperty('left', chRect.x);


    if (tipRect.width > window.innerWidth) {
      // Shrink tip
      tip.style.setProperty('white-space', 'normal');
      tip.style.setProperty('width', window.innerWidth);
    }
    tipRect = tip.getBoundingClientRect();
    if (tipRect.right > window.innerWidth) {
      // Shift tip to the left
      console.log('shift to left');
      tip.style.setProperty('left', tipRect.left - (tipRect.right - window.innerWidth));
    }
    tipRect = tip.getBoundingClientRect();
    if (tipRect.left < 0) {
      // Shift tip to the right
      tip.style.setProperty('left', 0);
    }

    // Highlight word in text
    const span = _getId(this.id);
    span.style.setProperty('background-color', 'gray');
  }
}


function mouseOut(event) {
  // this: ch span
  const tip = _getId('tip');
  tip.style.setProperty('visibility', 'hidden');

  // Highlight word in text
  // TODO: algo 2
  const span = _getId(this.id);
  span.style.removeProperty('background-color');
}


function SubmitClicked() {
  // Chunk and translate input, then display result
  if (!dict_loaded) {
    alert('Dict is not loaded yet. Please try again in a few seconds...');
    return;
  }
  const input = _getId('txt').value;
  const text_translator = new TextTranslator(dict);
  text_translator.SetText(input);
  const algorithm = _getId('algorithm').value;
  text_translator.SetVersion(algorithm);
  translated = text_translator.Translate();

  RenderResultText(translated);
}


function OnLoad() {
  // Load dict and parse it
  // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  const dict_parser = new DictParser();
  dict_parser.Load('data/cedict_ts.u8')
    .then((data) => {
      dict = data;
      dict_loaded = true;
  });
  CreateTooltip();

  // Submit button
  const submit = _getId('submit');
  submit.addEventListener('click', SubmitClicked);
}


function RenderResultText(text) {
  // translated: [
  //   [
  //     {char: 去, meanings: [ { trad, meanings: [], pinyin } ],},
  //     {char: 中國, meanings: [...],}
  //   ]
  // or
  // translated: [
  // [
  //   { char: "中", options: [...] },
  //   { char: "國", options: [...] },
  // ]
  const result = _getId('result');
  result.innerHTML = '';
  let i = 0;
  for (c of text) {
    let ch = document.createElement('ch');
    console.log(c);
    ch.innerHTML = c.char;
    ch.id = i;
    result.appendChild(ch);

    // Mouse actions
    if (c.hasOwnProperty('meanings')) {
      ch.addEventListener('mouseover', mouseOver, false);
      ch.addEventListener('mouseout', mouseOut, false);
    }
    i++;
  }
}
