// Init
// - Get dict into string
// - For each line, parse dict
//


// source: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
async function* makeTextFileLineIterator(fileURL) {
  const utf8Decoder = new TextDecoder('utf-8');
  const response = await fetch(fileURL);
  const reader = response.body.getReader();
  let { value: chunk, done: readerDone } = await reader.read();
  chunk = chunk ? utf8Decoder.decode(chunk) : '';

  const re = /\r\n/gm;
  let startIndex = 0;
  let result;

  for (;;) {
    let result = re.exec(chunk);
    if (!result) {
      if (readerDone) {
        break;
      }
      let remainder = chunk.substr(startIndex);
      ({ value: chunk, done: readerDone } = await reader.read());
      chunk = remainder + (chunk ? utf8Decoder.decode(chunk) : '');
      startIndex = re.lastIndex = 0;
      continue;
    }
    yield chunk.substring(startIndex, result.index);
    startIndex = re.lastIndex;
  }
  if (startIndex < chunk.length) {
    // last line didn't end in a newline char
    yield chunk.substr(startIndex);
  }
}

function processLine(line) {
  if (line.startsWith('#')
    || line.startsWith('%')
  ) {
    return;
  }
  const a = line.indexOf(' [');
  const b = line.indexOf('] ');
  const res = {
    'chars': line.substring(a, 0).split(' '),
    'pinyin': line.substring(a+2, b),
    'meaning': line.substring(b+3, line.length-1).split('/'),
  };
  return res;
}

var dict = [];
async function run() {
  //let urlOfFile = 'data/cedict_ts.u8';
  let urlOfFile = 'data/test_dict.u8';
  for await (let line of makeTextFileLineIterator(urlOfFile)) {
    var a = processLine(line);
    if (a != undefined) {
      dict.push(processLine(line));
    }
  }

  function getCandidates(str) {
    return dict.filter(function(ch) {
        for (c of ch['chars']) {
          if (c.startsWith(str)) return true;
        }
        return false;
    });
  }

  function printMatches(arr) {
    console.log('MATCH');
    for (j of arr) {
      console.log(j['chars']);
    }
  }

  const txt = '123451';
  console.log(dict);
  matchStr = undefined;
  divided = [];
  var prevMatch = [];
  for (ch of txt) {
    if (matchStr === undefined) {
      matchStr = ch;
    } else {
      matchStr = matchStr + ch;
    }
    matches = getCandidates(matchStr);
    console.log('matchStr:', matchStr, 'char:', ch, 'matches:', matches.length);
    if (matches.length == 0) {
      if (prevMatch.length == 1) {
        divided.push(prevMatch[0]);
      } else {
        console.log('ERROR: Most recent matches are +1');
      }
      printMatches(prevMatch);

      // Last string had no match, so we reset the string to a single char
      console.log('Check:', ch);
      matchStr = ch;
      matches = getCandidates(matchStr);
      if (matches.length == 0) {
        // This shouldn't happen. The char is not in the dictionary
        console.log('No match');
        matchStr = undefined;
        divided.push(ch);
      } else {
        prevMatch = matches;
        printMatches(prevMatch);
      }
    } else {
      prevMatch = matches;
    }
    //console.log('char:', ch);
    //console.log('matches:', getCandidates(matchStr).length);
  }
  console.log('-----');
  for (k of divided) {
    if (typeof(k) == 'object') {
      console.log(k['chars']);
    } else {
      console.log(k);
    }
  }

}

run();
