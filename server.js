var express = require('express');
var app = express();
var port = 4000;
app.use(express.static('public'))
app.get('/', (req, res) => res.sendFile(__dirname + '/public/index.html'))
app.get('/cedict', (req, res) => res.sendFile(__dirname + '/public/cedict.html'))

app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`);
});
