# zhtools

Tools for studying Mandarin

Demo: https://konradp.gitlab.io/zhtools

# Test
```
npm install
npm start
```

# TODO: 
Get char frequency stats from  
https://lingua.mtsu.edu/chinese-computing/statistics/char/listchangyong.php

# Example text
（台灣英文新聞/生活組 綜合報導）中央流行疫情指揮中心今(30)日公布國內新增3例本土COVID-19確定病例(案1127至案1129)，皆為機場防疫旅館:諾富特飯店員工。

指揮官陳時中指出，因為包括昨天確診的飯店主管(案1120)在內，目前累計已有4個案例，證實這的確屬於飯店群聚案例。至於關聯性, 則需檢驗基因序列來釐清。

# Studybook format
The studybook stores the texts which have been studied, the practice sessions
```
{
  title: 'Studybook for koradp.gitlab.io/zhtools',
  name: 'konradp',
  study_sessions: [
    {
      date: 2022-10-03 13:54,
      textId: '1cb251ec0d568de6a929b520c4aed8d1',
      charsPicked: [ '我', '是' ],
    },
  ],
  practice_sessions: {
    20221005: { 我: false, 是: true, ... },
    20221006: { .... },
  },
  known_chars_words: [
    { char: '我', proficiency: 1 },
    { char: '是', proficiency: 10 },
  ],
  texts: [
    {
      id: '1cb251ec0d568de6a929b520c4aed8d1',
      title: '...',
      content: '...',
      type: 'rss/pasted',
      date: 2022-10....,
    },
  ],
}
```
legend:

- sessions.textId: MD5 hash of the text
- texts.date: date of RSS feed or date of the paste
- texts.id: MD5 hash of the text, to avoid storing
  the same text multiple times
- texts.title: first 5 chars of the text

# Practice algorithm
TODO:

- How to pick which chars to practice?
- What to store and how? Should we keep when the character was
  most recently reviewed? Should recently learned chars be first
  on the list to review? Should recently learned chars appear
  more frequently in reviews?

What should make a char appear on review:
- Recently learned
- Not recently practiced
- Practice score is less than 100% (practice score is 100% when it has been
  guessed correctly 5 times consecutively)

## Simplified SM-2
Modified algorithm, instead of grading cards from 0 to 5, we grade it as 0/1 (incorrect/correct).
Each character stores info:

- n: repetition number: # of times card has been successfully recalled in a row since last time it was not
- I: inter-repetition interval (in days)

algorithm, adapted from https://en.wikipedia.org/wiki/SuperMemo:
```
  if q == true (correct response):
    if n = 0: I = 1
    else:     I = I*2
    n++
  else (incorrect response):
    n = 0
    I = 1
```
also, the last review session number is stored.

**Note:** Original algorithm (q from 0 to 5).  
```
EF = EF + 0.1 − (5 − q)*(0.08 + (5 − q)*0.02)
```
so values are

q | EF
-- | --
0 | EF - 0.88
1 | EF - 0.54
2 | EF - 0.32
3 | EF - 0.14
4 | EF
5 | EF + 0.1

# Demo transcript
Notes for a demo video

## Workflow
- Open a page in Mandarin: Taiwan news, copy text
- Go to app, load studybook
- Paste text
- Press Analyse, see:
  - View
  - Stats

## Motivation: closing
- I'd like to read difficult articles now, but:
  - I get stuck when going char by char
  - if I just use google translate, I don't learn anything
    because I just read the translation
  - no metrics for progress
  - not able to find:
    - which chars are common, and how common they are
  - I don't want to waste time on chars which I'll rarely use
    but focus on the chars which help me advance fast

# Acknowledgements
https://mandarinspot.com/  
https://sensiblechinese.com/common-chinese-characters/
CC-CEDICT
